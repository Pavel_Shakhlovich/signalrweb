using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalRWebPack.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        static User[] users = new User[]
        {
            new User (1, "Alice", true),
            new User (2, "Bob", true),
            new User (3, "Chuck", false),
        };

        public async Task NewMessage(string username, string message)
        {

            await Clients.All.SendAsync("messageReceived", username, message);
        }

        public async Task Login(string username) // todo authentification logic
        {
            if (TryLogin(username))
            {
                await AddToGroup("Access");
                await Clients.User(Context.UserIdentifier).SendAsync("loginSuccessful", username,
                    $"{Context.UserIdentifier} has successfuly logged in.");
            }
        }

        private bool TryLogin(string username)
        {
            foreach (User user in users)
            {
                if (user.Login.Equals(username))
                {
                    return true;
                }
            }
            return false;
        }

        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
            await Clients.Group(groupName).SendAsync("Send", $"{Context.UserIdentifier} has joined the group {groupName}.");
        }

        public async Task RemoveFromGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);

            await Clients.Group(groupName).SendAsync("Send", $"{Context.UserIdentifier} has left the group {groupName}.");
        }
    }

    class User
    {
        public User(int id, string login, bool isAlowed)
        {
            Id = id;
            Login = login;
            IsAlowed = isAlowed;
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public bool IsAlowed { get; set; }
    }
}
