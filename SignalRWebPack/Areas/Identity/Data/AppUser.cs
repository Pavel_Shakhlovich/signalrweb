﻿using Microsoft.AspNetCore.Identity;

namespace SignalRWebPack.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the User class
    public class AppUser : IdentityUser
    {
    }
}
