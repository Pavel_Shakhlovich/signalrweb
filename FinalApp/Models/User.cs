﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalApp.Models
{
    public class User : IdentityUser<int>
    {

    }

    public class AppIdentityRole : IdentityRole<int>
    {

    }
}
