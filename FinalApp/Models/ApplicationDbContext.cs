﻿using System;
using System.Collections.Generic;
using System.Text;
using FinalApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FinalApp.Models
{
    public class ApplicationDbContext : IdentityDbContext<User, AppIdentityRole, int>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();   // clear db
            //Database.EnsureCreated();   // recreate dbs   
        }
    }
}
