﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace FinalApp.Hubs
{
    public class ChatHub : Hub
    {
        private const string GROUP_NAME = "Cats";
        private const string ACCESS_DENIED_MESSAGE = "Sorry, you don't have a permission to write messages";

        public override async Task OnConnectedAsync()
        {
            var userId = Context.UserIdentifier;

            if (userId != "3")
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, GROUP_NAME);
            }
            await Clients.Caller.SendAsync("notify", $"Welcome, {Context.User.Identity.Name}!");
            await base.OnConnectedAsync();

        }

        public async Task NewMessage(string message)
        {
            var userId = Context.UserIdentifier;

            if (userId == "3")
            {
                await Clients.User(userId).SendAsync("notify", ACCESS_DENIED_MESSAGE);
            }
            else
            {
                await Clients.Group(GROUP_NAME).SendAsync("messageReceived", Context.User.Identity.Name, message);
            }
        }
    }
}