"use strict";
var divMessages = document.querySelector("#divMessages");
var tbMessage = document.querySelector("#tbMessage");
var btnSend = document.querySelector("#btnSend");
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/chathub").build();

connection.on("messageReceived", function (username, message) {
    var messages = document.createElement("div");
    messages.innerHTML =
        "<div class=\"message-author\">" + username + "</div><div>" + message + "</div>";
    divMessages.appendChild(messages);
    divMessages.scrollTop = divMessages.scrollHeight;
});

connection.on("notify", function (message) {
    var messages = document.createElement("div");
    messages.innerHTML =
        "<div>" + message + "</div>";
    divMessages.appendChild(messages);
    divMessages.scrollTop = divMessages.scrollHeight;
});

connection.start().catch(function (err) { return document.write(err); });

tbMessage.addEventListener("keyup", function (e) {
    if (e.key === "Enter") {
        send();
    }
});

btnSend.addEventListener("click", send);

function send() {
    connection.send("newMessage", tbMessage.value)
        .then(function () { return tbMessage.value = ""; });
}